use crate::cli;

pub fn from(media: &cli::Media) -> Vec<&'static str> {
    match media {
        cli::Media::Image => vec!["bmp", "jpg", "jpeg", "png", "gif", "webp", "jfif", "heif"],
        cli::Media::Video => vec![
            "avi", "flv", "m4v", "mkv", "mov", "mp4", "mpeg", "mpg", "rm", "ts", "webm", "wmv",
        ],
    }
}
