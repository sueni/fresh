mod checker;
mod cli;
mod collector;
mod extensions;
mod item;
mod sorter;

use collector::Collector;
use item::Item;
use sorter::Sorter;
use std::io::{self, Write};
use std::os::unix::ffi::OsStrExt;

pub fn display(items: &[Item]) -> io::Result<()> {
    let stdout = io::stdout();
    let mut stdout = stdout.lock();
    for item in items {
        stdout.write_all(item.path.as_os_str().as_bytes())?;
        stdout.write_all(b"\n")?;
    }
    Ok(())
}

fn main() {
    let args = match cli::parse_args() {
        Ok(args) => args,
        Err(err) => {
            eprintln!("ERROR: {}\n{}", err, cli::HELP);
            std::process::exit(1);
        }
    };

    let extensions = extensions::from(&args.media);
    let checker = checker::build_from(&args.mime, extensions);
    let sorter = Sorter::new(args.reverse, args.shuffle);

    let collector = Collector::build_from(&args);
    let mut pool = collector.collect_with(&checker);
    sorter.sort(&mut pool, &args.srtmode);
    display(&pool).unwrap();
}
