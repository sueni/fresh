use std::path::{Path, PathBuf};
use std::{fs, io};

use crate::checker::Checker;
use crate::cli;
use crate::item::Item;

pub struct Collector<'a> {
    pub dir: &'a Path,
    pub pool: Vec<Item>,
    pub recursive: bool,
    pub smart_recursive: bool,
}

impl<'a> Collector<'a> {
    pub fn build_from(args: &'a cli::Args) -> Self {
        let mut collector = Self::new(&args.dir);

        if let Some(recursion) = &args.recursion {
            match recursion {
                cli::Recursion::Smart => collector.smart_recursive = true,
                cli::Recursion::Deep => collector.recursive = true,
            }
        }
        collector
    }

    pub fn new(dir: &'a Path) -> Self {
        Self {
            dir,
            pool: Vec::new(),
            recursive: false,
            smart_recursive: false,
        }
    }

    pub fn collect_with(mut self, checker: &Checker) -> Vec<Item> {
        if self.recursive {
            self.traverse_recursive(self.dir, checker).unwrap()
        } else if self.smart_recursive {
            self.traverse_smart_recursive(self.dir, checker).unwrap()
        } else {
            self.traverse_plain(self.dir, checker).unwrap();
        }
        self.pool
    }

    fn traverse_recursive(&mut self, path: &Path, checker: &Checker) -> io::Result<()> {
        if let Ok(dir) = fs::read_dir(path) {
            for entry in dir {
                let path = entry?.path();
                if path.is_dir() {
                    self.traverse_recursive(&path, checker)?;
                } else if path.is_file() && checker.check(&path) {
                    self.keep(path);
                }
            }
        }
        Ok(())
    }

    fn traverse_smart_recursive(&mut self, path: &Path, checker: &Checker) -> io::Result<()> {
        let mut current_dirs: Vec<PathBuf> = Vec::new();
        let mut candidate_dirs: Vec<PathBuf> = Vec::new();

        self.walk(path, checker, &mut current_dirs)?;

        while self.pool.is_empty() && !current_dirs.is_empty() {
            for path in current_dirs.drain(..) {
                self.walk(&path, checker, &mut candidate_dirs)?;
            }
            current_dirs.append(&mut candidate_dirs);
        }

        Ok(())
    }

    fn walk(&mut self, path: &Path, checker: &Checker, dirs: &mut Vec<PathBuf>) -> io::Result<()> {
        if let Ok(dir) = fs::read_dir(path) {
            for entry in dir {
                let path = entry?.path();
                if path.is_dir() {
                    dirs.push(path.to_owned());
                } else if path.is_file() && checker.check(&path) {
                    self.keep(path);
                }
            }
        }
        Ok(())
    }

    fn traverse_plain(&mut self, path: &Path, checker: &Checker) -> io::Result<()> {
        if let Ok(dir) = fs::read_dir(path) {
            for entry in dir {
                let path = entry?.path();
                if path.is_file() && checker.check(&path) {
                    self.keep(path);
                }
            }
        }
        Ok(())
    }

    fn keep(&mut self, path: PathBuf) {
        let metadata = path.metadata().unwrap();
        let item = Item {
            path,
            mtime: metadata.modified().unwrap(),
            size: metadata.len(),
        };
        self.pool.push(item);
    }
}
