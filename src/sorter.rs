use crate::cli::Course;
use crate::cli::SortingMode;
use crate::item::Item;
use rand::seq::SliceRandom;
use rand::thread_rng;

pub struct Sorter {
    reverse: bool,
    shuffle: bool,
}

impl Sorter {
    pub fn new(reverse: bool, shuffle: bool) -> Self {
        Self { reverse, shuffle }
    }

    pub fn sort(&self, pool: &mut Vec<Item>, srtmode: &Option<SortingMode>) {
        if self.shuffle {
            return pool.shuffle(&mut thread_rng());
        }

        if let Some(srtmode) = srtmode {
            match srtmode {
                SortingMode::Abc(Course::Forward) => sort_abc_forward(pool),
                SortingMode::Abc(Course::Backward) => sort_abc_backward(pool),
                SortingMode::Mtime(Course::Forward) => sort_mtime_forward(pool),
                SortingMode::Mtime(Course::Backward) => sort_mtime_backward(pool),
                SortingMode::Size(Course::Forward) => sort_size_forward(pool),
                SortingMode::Size(Course::Backward) => sort_size_backward(pool),
            }
            if self.reverse {
                pool.reverse()
            }
        }
    }
}

fn sort_abc_forward(pool: &mut Vec<Item>) {
    pool.sort_unstable_by(|a, b| alphanumeric_sort::compare_path(&a.path, &b.path));
}

fn sort_abc_backward(pool: &mut Vec<Item>) {
    pool.sort_unstable_by(|a, b| alphanumeric_sort::compare_path(&b.path, &a.path));
}

fn sort_mtime_forward(pool: &mut Vec<Item>) {
    pool.sort_unstable_by(|a, b| a.mtime.cmp(&b.mtime));
}

fn sort_mtime_backward(pool: &mut Vec<Item>) {
    pool.sort_unstable_by(|a, b| b.mtime.cmp(&a.mtime));
}

fn sort_size_forward(pool: &mut Vec<Item>) {
    pool.sort_unstable_by(|a, b| a.size.cmp(&b.size));
}

fn sort_size_backward(pool: &mut Vec<Item>) {
    pool.sort_unstable_by(|a, b| b.size.cmp(&a.size));
}
