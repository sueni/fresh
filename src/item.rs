use std::path::PathBuf;
use std::time::SystemTime;

pub struct Item {
    pub path: PathBuf,
    pub mtime: SystemTime,
    pub size: u64,
}
