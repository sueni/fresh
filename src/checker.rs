use crate::cli::Mime;
use infer::Infer;
use std::path::Path;

pub trait Check {
    fn check(&self, path: &Path) -> bool;
}

pub struct Checker {
    pub checker: Box<dyn Check>,
}

impl Checker {
    pub fn check(&self, path: &Path) -> bool {
        self.checker.check(path)
    }
}

pub struct ForcedMimeChecker {
    info: Infer,
    extensions: Vec<&'static str>,
}

impl Check for ForcedMimeChecker {
    fn check(&self, path: &Path) -> bool {
        match self.info.get_from_path(path) {
            Ok(Some(info)) => self.extensions.contains(&info.extension()),
            Ok(None) => false,
            Err(_) => false,
        }
    }
}

pub struct FallbackMimeChecker {
    extensions: Vec<&'static str>,
    info: Infer,
}

impl Check for FallbackMimeChecker {
    fn check(&self, path: &Path) -> bool {
        match &path.extension() {
            Some(ext) => self
                .extensions
                .contains(&ext.to_ascii_lowercase().to_str().unwrap()),
            None => match self.info.get_from_path(path) {
                Ok(Some(info)) => self.extensions.contains(&info.extension()),
                Ok(None) => false,
                Err(_) => false,
            },
        }
    }
}

pub struct NoMimeChecker {
    extensions: Vec<&'static str>,
}

impl Check for NoMimeChecker {
    fn check(&self, path: &Path) -> bool {
        match &path.extension() {
            Some(ext) => self
                .extensions
                .contains(&ext.to_ascii_lowercase().to_str().unwrap()),
            None => false,
        }
    }
}

pub fn build_from(mime: &Option<Mime>, extensions: Vec<&'static str>) -> Checker {
    match mime {
        Some(Mime::Forced) => Checker {
            checker: Box::new(ForcedMimeChecker {
                info: Infer::new(),
                extensions,
            }),
        },
        Some(Mime::Fallback) => Checker {
            checker: Box::new(FallbackMimeChecker {
                info: Infer::new(),
                extensions,
            }),
        },
        None => Checker {
            checker: Box::new(NoMimeChecker { extensions }),
        },
    }
}
