use std::ffi::OsString;
use std::path::PathBuf;

pub const HELP: &str = "
USAGE:
  fresh [FLAGS] [OPTIONS] --media <TYPE>

FLAGS:
  -h                      Show help
      --reverse
      --shuffle

REQUIRED OPTIONS:
      --media <TYPE>      [values: image, video]

OPTIONS:
      --dir <DIR>
      --mime <MODE>       [values: forced, fallback]
      --recursion <MODE>  [values: smart, deep]
      --sorting <MODE>    [values: abc-<frw | bkwr>, mtime-<frw | bkwr>, size-<frw | bkwr>]";

pub enum Media {
    Image,
    Video,
}

impl TryFrom<OsString> for Media {
    type Error = ArgError;

    fn try_from(value: OsString) -> Result<Self, Self::Error> {
        match value.to_str() {
            Some("image") => Ok(Media::Image),
            Some("video") => Ok(Media::Video),
            Some(other) => Err(ArgError::InvalidMedia(other.to_string())),
            None => unreachable!(),
        }
    }
}

pub enum Mime {
    Forced,
    Fallback,
}

impl TryFrom<OsString> for Mime {
    type Error = ArgError;

    fn try_from(value: OsString) -> Result<Self, Self::Error> {
        match value.to_str() {
            Some("forced") => Ok(Mime::Forced),
            Some("fallback") => Ok(Mime::Fallback),
            Some(other) => Err(ArgError::InvalidMime(other.to_string())),
            None => unreachable!(),
        }
    }
}

pub enum Recursion {
    Smart,
    Deep,
}

impl TryFrom<OsString> for Recursion {
    type Error = ArgError;

    fn try_from(value: OsString) -> Result<Self, Self::Error> {
        match value.to_str() {
            Some("smart") => Ok(Recursion::Smart),
            Some("deep") => Ok(Recursion::Deep),
            Some(other) => Err(ArgError::InvalidRecursion(other.to_string())),
            None => unreachable!(),
        }
    }
}

pub enum Course {
    Forward,
    Backward,
}

pub enum SortingMode {
    Abc(Course),
    Mtime(Course),
    Size(Course),
}

impl TryFrom<OsString> for SortingMode {
    type Error = ();

    fn try_from(value: OsString) -> Result<Self, Self::Error> {
        match value.to_str() {
            Some("abc-frw") => Ok(Self::Abc(Course::Forward)),
            Some("abc-bkwr") => Ok(Self::Abc(Course::Backward)),
            Some("mtime-frw") => Ok(Self::Mtime(Course::Forward)),
            Some("mtime-bkwr") => Ok(Self::Mtime(Course::Backward)),
            Some("size-frw") => Ok(Self::Size(Course::Forward)),
            Some("size-bkwr") => Ok(Self::Size(Course::Backward)),
            _ => Err(()),
        }
    }
}

pub struct Args {
    pub reverse: bool,
    pub shuffle: bool,
    pub dir: PathBuf,
    pub media: Media,
    pub mime: Option<Mime>,
    pub recursion: Option<Recursion>,
    pub srtmode: Option<SortingMode>,
}

pub fn parse_args() -> Result<Args, lexopt::Error> {
    use lexopt::prelude::*;

    let mut reverse = false;
    let mut shuffle = false;
    let mut dir = PathBuf::from(".");
    let mut media: Result<Media, ArgError> = Err(ArgError::MissingMedia);
    let mut mime: Option<Mime> = None;
    let mut recursion: Option<Recursion> = None;
    let mut srtmode: Option<SortingMode> = None;
    let mut parser = lexopt::Parser::from_env();

    while let Some(arg) = parser.next()? {
        match arg {
            Long("reverse") => reverse = true,
            Long("shuffle") => shuffle = true,
            Long("dir") => {
                dir = parser.value()?.into();
                if !dir.is_dir() {
                    return Err(lexopt::Error::Custom(Box::new(ArgError::InvalidDir(dir))));
                }
            }
            Long("sorting") => srtmode = parser.value()?.try_into().ok(),
            Long("media") => media = parser.value()?.try_into(),
            Long("mime") => {
                mime = match parser.value()?.try_into() {
                    Ok(m) => Some(m),
                    Err(e) => return Err(lexopt::Error::Custom(Box::new(e))),
                }
            }
            Long("recursion") => {
                recursion = match parser.value()?.try_into() {
                    Ok(m) => Some(m),
                    Err(e) => return Err(lexopt::Error::Custom(Box::new(e))),
                }
            }
            Short('h') => {
                println!("{}", HELP);
                std::process::exit(0);
            }
            _ => return Err(arg.unexpected()),
        }
    }

    if reverse && shuffle {
        return Err(lexopt::Error::from(
            "mix of options --shuffle and --reverse",
        ));
    }

    if srtmode.is_some() && shuffle {
        return Err(lexopt::Error::from(
            "mix of options --shuffle and --sorting",
        ));
    }

    if reverse && srtmode.is_none() {
        return Err(lexopt::Error::from(
            "found option --reverse with no corresponding --sorting",
        ));
    }

    let media = match media {
        Ok(m) => m,
        Err(e) => return Err(lexopt::Error::Custom(Box::new(e))),
    };

    Ok(Args {
        reverse,
        shuffle,
        dir,
        media,
        mime,
        recursion,
        srtmode,
    })
}

use std::{error::Error, fmt};

#[derive(Debug)]
pub enum ArgError {
    InvalidDir(std::path::PathBuf),
    MissingMedia,
    InvalidMedia(String),
    InvalidMime(String),
    InvalidRecursion(String),
}

impl Error for ArgError {}

impl fmt::Display for ArgError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ArgError::InvalidDir(path) => write!(f, "invalid directory: {}", path.display()),
            ArgError::MissingMedia => write!(f, "missing required option --media <TYPE>"),
            ArgError::InvalidMedia(mtype) => write!(f, "invalid media type: {}", mtype),
            ArgError::InvalidMime(mode) => write!(f, "invalid mime mode: {}", mode),
            ArgError::InvalidRecursion(mode) => {
                write!(f, "invalid recursion mode: {}", mode)
            }
        }
    }
}
